import React, { Component } from 'react';
import './App.css';
import { sampleText } from './sampleText'
import marked from 'marked'

class App extends Component {
  state = {
    text: sampleText
  }

  // Récupérer et maj la valeur de l'input à chaque action de l'user
  handleChange = event => {
    const text = event.target.value
    this.setState({ text })
  }

  // Cycle de vie "quand le component est monté"
  componentDidMount() {
    const text = localStorage.getItem('text')
    
    if(text) {
      this.setState({ text })
    } else {
      this.setState({ text: sampleText})
    }
    
  }

  // Cycle de vie "quand le component il est mis à jour"
  componentDidUpdate() {
    const text = this.state.text
    localStorage.setItem('text', text)
  }

  renderText = text => marked(text, { sanitize: true })

  render () {
    return (
      <div className="container">
        <h1 className="display-3 mb-3" align="center">Editeur de Markdown</h1>
        <div className="divider"></div>
        <div className="row">
          <div className="col-sm-6">
            <textarea
              onChange={this.handleChange}
              value={this.state.text}
              className="form-control"
              rows="25">
            </textarea>
          </div>
          <div className="text2 col-sm-6">
            <div dangerouslySetInnerHTML={{ __html: this.renderText(this.state.text)}}>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default App;
